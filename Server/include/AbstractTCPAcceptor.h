#ifndef ABSTRACTTCPACCEPTOR_H
#define ABSTRACTTCPACCEPTOR_H

#include "Parameters.h"

namespace Server {

class AbstractDataProvider;
class AbstractResponseObject;

class AbstractTCPAcceptor
{
public:
    AbstractTCPAcceptor(const Parameters &params);
    virtual ~AbstractTCPAcceptor();

    int run();

    virtual bool isConnected() const = 0;

    virtual void abort() = 0;

protected:
    const Parameters &getParameters() const;

private:
    virtual void bindToAddress() = 0;
    virtual int processClientsRequests() = 0;
    virtual int lastErrorCode() = 0;

private:
    Parameters m_params;
};

}

#endif // ABSTRACTTCPACCEPTOR_H
