#ifndef CLIENTPARAMETERS_H
#define CLIENTPARAMETERS_H

#include <string>

namespace Server {

struct Parameters
{
    std::string bindAddr;
};

}

#endif // CLIENTPARAMETERS_H
