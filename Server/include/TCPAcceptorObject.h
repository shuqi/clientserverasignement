#ifndef TCPACCEPTOROBJECT_H
#define TCPACCEPTOROBJECT_H

#include "AbstractTCPAcceptor.h"

namespace Server {

class TCPAcceptorObject : public AbstractTCPAcceptor
{
public:
    TCPAcceptorObject(const Parameters &params);
    virtual ~TCPAcceptorObject();

    bool isConnected() const;

    void abort();

private:
    void bindToAddress();
    int processClientsRequests();
    int lastErrorCode();

private:
    void *m_context;
    void *m_server;
    bool m_connected;
};

}

#endif // TCPACCEPTOROBJECT_H
