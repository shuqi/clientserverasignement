message("Including externals.cmake ...")

set(PROTOBUF_INCLUDE "${EXTERNALS_ROOT}/protobuf-2.6.0/src")
set(ZEROMQ_INCLUDE "${EXTERNALS_ROOT}/zeromq-4.0.5/include")

if(WIN32)
    set(PROTOBUF_LIB_PATH "${EXTERNALS_ROOT}/protobuf-2.6.0/vsprojects/${LONG_TARGET_POSTFIX}")
    set(ZEROMQ_LIB_PATH "${EXTERNALS_ROOT}/zeromq-4.0.5/bin/Win32")
elseif(UNIX)
    set(PROTOBUF_LIB_PATH "${EXTERNALS_ROOT}/protobuf-2.6.0/src/.libs")
    set(ZEROMQ_LIB_PATH "${EXTERNALS_ROOT}/zeromq-4.0.5/src/.libs")
endif()

set(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH}
    ${PROTOBUF_LIB_PATH}
    ${ZEROMQ_LIB_PATH}
    )

if(WIN32)
    find_library(Protobuf_LIB libprotobuf)
    
    if(APP_COMPILE_TYPE STREQUAL debug)
        find_library(ZeroMQ_LIB libzmq_${TARGET_POSTFIX})
    else()
        find_library(ZeroMQ_LIB libzmq)
    endif()
    
elseif(UNIX)
    find_library(Protobuf_LIB protobuf)
    find_library(ZeroMQ_LIB zmq)
endif()

message("Protobuf library path is ${PROTOBUF_LIB_PATH}")
message("ZeroMQ library path is ${ZEROMQ_LIB_PATH}")

