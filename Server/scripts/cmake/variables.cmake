message("Including variables.cmake ...")

if(NOT DEFINED BUILD_ID)
    set(BUILD_ID "\"none\"")
endif()

if(NOT DEFINED APP_VERSION)
    set(APP_VERSION "\"1.0.0\"")
endif()

if(NOT DEFINED APP_BUILD_TYPE)
    set(APP_BUILD_TYPE "\"DEV\"")
endif()

if(APP_BUILD_TYPE STREQUAL "\"DEV\"")
    message("Building for developers")
    add_definitions(-DAPP_BUILD_TYPE_DEV)
endif()

# Setting the APP_COMPILE_TYPE (release or debug), used for constructing file paths depending on the CMAKE_BUILD_TYPE.
if(CMAKE_BUILD_TYPE)
    if(CMAKE_BUILD_TYPE STREQUAL Release OR CMAKE_BUILD_TYPE STREQUAL RelWithDebInfo OR CMAKE_BUILD_TYPE STREQUAL MinSizeRel)
        set(APP_COMPILE_TYPE release)
    else()
        set(APP_COMPILE_TYPE debug)
    endif()
else()
    set(APP_COMPILE_TYPE debug)
endif()

if(WIN32)
    set(PLATFORM_DIR win)
elseif(APPLE)
    set(PLATFORM_DIR mac)
else(APPLE)
    set(PLATFORM_DIR unix)
endif()

set(TARGET_POSTFIX "")

if(APP_COMPILE_TYPE STREQUAL debug)
   set(TARGET_POSTFIX "d")
   set(LONG_TARGET_POSTFIX "debug")
else()
   set(LONG_TARGET_POSTFIX "release")
endif()

set(EXTERNALS_ROOT "${APP_ROOT}/../externals")
set(COMMON_ROOT "${APP_ROOT}/../common")
set(PROTOBUF_GENERATED_FILES_ROOT "${APP_ROOT}/../generated_by_protobuf")
