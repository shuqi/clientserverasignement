#include "AbstractTCPAcceptor.h"

#include <iostream>

static const int BUF_SIZE = 2048;

using namespace Server;

AbstractTCPAcceptor::AbstractTCPAcceptor(const Parameters &params) :
    m_params(params)
{

}

const Parameters &AbstractTCPAcceptor::getParameters() const
{
    return m_params;
}

int AbstractTCPAcceptor::run()
{
    if (!isConnected())
        bindToAddress();

    if (isConnected()) {

        std::cout << "Server connected" << std::endl;

        // While connected process clients requests.
        while (isConnected()) {
            int result = processClientsRequests();
            if (result != 0)
                return result;
        }

        return 0;
    }

    return lastErrorCode();
}

AbstractTCPAcceptor::~AbstractTCPAcceptor()
{

}
