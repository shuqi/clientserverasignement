#include "TCPAcceptorObject.h"

using namespace Server;

int main(int argc, char **argv)
{
    Parameters params;
    params.bindAddr = "tcp://*:5687";

    TCPAcceptorObject server(params);
    return server.run();
}
