#include "TCPAcceptorObject.h"
#include "account.pb.h"

#include <zmq.h>
#include <errno.h>
#include <iostream>

using namespace Server;

static const int BUF_SIZE = 2048;

TCPAcceptorObject::TCPAcceptorObject(const Parameters &params) :
    AbstractTCPAcceptor(params),
    m_context(NULL), m_server(NULL), m_connected(false)
{

}

bool TCPAcceptorObject::isConnected() const
{
    return m_connected && m_context && m_server;
}

int TCPAcceptorObject::lastErrorCode()
{
    return zmq_errno();
}

void TCPAcceptorObject::abort()
{
    if (m_server)
        zmq_close(m_server);

    if (m_context)
        zmq_ctx_destroy(m_context);

    m_context = NULL;
    m_server = NULL;
    m_connected = false;
}

void TCPAcceptorObject::bindToAddress()
{
    if (m_context == NULL)
        m_context = zmq_ctx_new();

    if (m_server == NULL)
        m_server = zmq_socket(m_context, ZMQ_REP);

    m_connected = (zmq_bind(m_server, getParameters().bindAddr.c_str()) == 0);
}

int TCPAcceptorObject::processClientsRequests()
{
    char buf_recv[BUF_SIZE];
    int sz = zmq_recv(m_server, buf_recv, BUF_SIZE, 0);

    if (sz == -1)
        return lastErrorCode();

    Account acc;

    if (acc.ParseFromArray(buf_recv, sz)) {

        std::cout << "Request from client: " << acc.id() << '\t' << acc.clientname() << '\t' << acc.balance() << std::endl;

        // Giving back to clients 200 more $ :)
        acc.set_balance(acc.balance() + 200);

        char buf_send[BUF_SIZE];
        int accSz = acc.ByteSize();
        acc.SerializeToArray(buf_send, accSz);

        zmq_send(m_server, buf_send, accSz, 0);

        return 0;
    }

    return -1;
}

TCPAcceptorObject::~TCPAcceptorObject()
{
    abort();
}
