Pre-requisites

1) CMake 2.8.11 or higher;
2) Visual Studio 2010 on windows. GCC for Unix;
3) Boost libraries (http://sourceforge.net/projects/boost/files/boost/1.57.0/boost_1_57_0.tar.bz2/download). The package has to be un-compressed in the externals folder altogether with protobuf and zeromq libraries.
To compile the Boost please run the boostrap script of the corresponding platform then ./b2
4) On Linux the protobuf and zeromq libraries have to be build (./configure in the root folders then make). On Windows I have compiled already debug and release libraries with MSVC 2010;

Once the pre-requisites are met simply run the build_client / build_server scripts. They will generate Server and Client binaries in the bin folder.