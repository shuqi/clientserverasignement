#include "ResponseObject.h"

#include <zmq.h>
#include <cstdlib>
#include <cstring>

using namespace Client;

static const int BUF_SIZE = 2048;

ResponseObject::ResponseObject(void *requester) :
    m_requester(requester), m_buf_recv(NULL), m_lastReadSz(0)

{
    m_buf_recv = malloc(BUF_SIZE);
}

bool ResponseObject::send(const void *data, int sz)
{
    int result = zmq_send(m_requester, data, sz, 0);

    if (result == -1)
        return false;

    m_lastReadSz = zmq_recv(m_requester, m_buf_recv, BUF_SIZE, 0);

    if (m_lastReadSz < 0)
        return false;

    return true;
}

void ResponseObject::getResponse(void *data, int *sz)
{
    if (m_lastReadSz > 0) {
        *sz = m_lastReadSz;
        memcpy(data, m_buf_recv, m_lastReadSz);
    }
}

ResponseObject::~ResponseObject()
{
    free(m_buf_recv);
}
