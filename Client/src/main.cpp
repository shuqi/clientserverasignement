#include "TCPConnectorObject.h"

using namespace Client;

int main(int argc, char **argv)
{
    Parameters params;
    params.connectAddr = "tcp://localhost:5687";

    TCPConnectorObject client(params);
    return client.run();
}
