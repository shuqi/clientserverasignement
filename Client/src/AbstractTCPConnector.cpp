#include "AbstractTCPConnector.h"
#include "AbstractDataProvider.h"
#include "AbstractResponseObject.h"

static const int BUF_SIZE = 2048;

using namespace Client;

AbstractTCPConnector::AbstractTCPConnector(const Parameters &params) :
    m_params(params), m_dataProvider(NULL), m_response(NULL)
{

}

const Parameters &AbstractTCPConnector::getParameters() const
{
    return m_params;
}

int AbstractTCPConnector::run()
{
    if (!isConnected())
        connectToServer();

    if (m_response && m_dataProvider && m_dataProvider->open() && isConnected()) {

        int result = -1;
        char buf_send[BUF_SIZE];
        char buf_recv[BUF_SIZE];
        int sz = BUF_SIZE;

        while (isConnected() && ((result = m_dataProvider->read(buf_send, &sz)) == 0)) {

            if (sz > 0 && m_response->send(buf_send, sz)) {
                m_response->getResponse(buf_recv, &sz);
                processResponse(buf_recv, sz);
                sz = BUF_SIZE;
            }
            else {
                return -1;
            }
        }

        return result;
    }

    return -1;
}

void AbstractTCPConnector::setDataProvider(AbstractDataProvider *dataProvider)
{
    m_dataProvider = dataProvider;
}

void AbstractTCPConnector::setResponseObject(AbstractResponseObject *response)
{
    m_response = response;
}

AbstractTCPConnector::~AbstractTCPConnector()
{

}
