#include "FakeDataProvider.h"

#include <exception>

using namespace Client;

struct DataProviderException : public std::exception
{
    DataProviderException(const char *description) :
        m_description(description)
    {

    }

    virtual ~DataProviderException() throw()
    {

    }

    const char *what() const throw()
    {
        return m_description.c_str();
    }

private:
    std::string m_description;
};

FakeDataProvider::FakeDataProvider() :
    m_thread(NULL), m_open(false), m_canRead(true)
{

}

bool FakeDataProvider::open()
{
    if (!isOpen()) {
        m_open = true;

        if (m_thread == NULL)
            m_thread = new boost::thread(&FakeDataProvider::run, this);
    }

    return m_open;
}

bool FakeDataProvider::isOpen()
{
    return m_open && m_thread;
}

int FakeDataProvider::read(void *data, int *sz)
{
    boost::mutex::scoped_lock lock(m_mutex);

    if (isOpen()) {

        // Wait indefintely until something is available.
        while (m_accounts.empty())
            m_condition.wait(lock);

        if (!m_accounts.empty()) {
            const Account &acc = m_accounts.front();

            const int accSz = acc.ByteSize();

            if (accSz > *sz)
                return -2;

            acc.SerializeToArray(data, accSz);
            *sz = accSz;

            m_accounts.pop();
        }
        else {
            *sz = 0;
        }

        return 0;
    }

    return -1;
}

void FakeDataProvider::close()
{
    if (isOpen()) {
        boost::mutex::scoped_lock lock(m_mutex);
        m_open = false;
        m_condition.notify_one();

        if (m_thread) {
            m_thread->interrupt();
            m_thread->join();
            delete m_thread; m_thread = NULL;
        }
    }
}

void FakeDataProvider::run()
{
    try {
        Account acc;

        acc.set_id(1);
        acc.set_balance(452.34);
        acc.set_clientname("Bob Marlon");
        addAccount(acc);

        acc.set_id(2);
        acc.set_balance(1452.34);
        acc.set_clientname("Ashely Dude");
        addAccount(acc);

        acc.set_id(3);
        acc.set_balance(6365.25);
        acc.set_clientname("Michael Jackson");
        addAccount(acc);

        acc.set_id(4);
        acc.set_balance(512.00);
        acc.set_clientname("Power Two");
        addAccount(acc);

        acc.set_id(-5000000);
        acc.set_balance(-512635625.25);
        acc.set_clientname("Negative Numbers");
        addAccount(acc);

        acc.set_id(5000000);
        acc.set_balance(512635625.25);
        acc.set_clientname("Positive Numbers");
        addAccount(acc);

        boost::mutex::scoped_lock lock(m_mutex);
        m_condition.notify_one();
    }
    catch(const DataProviderException &ex) {
        std::cerr << ex.what() << std::endl;
    }
}

void FakeDataProvider::addAccount(const Account &acc)
{
    if (isOpen()) {
        boost::mutex::scoped_lock lock(m_mutex);
        m_accounts.push(acc);
        m_condition.notify_one();
    }
    else {
        throw(DataProviderException("Data provider is closed."));
    }
}

FakeDataProvider::~FakeDataProvider()
{
    close();
}
