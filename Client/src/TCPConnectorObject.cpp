#include "TCPConnectorObject.h"
#include "FakeDataProvider.h"
#include "ResponseObject.h"

#include <zmq.h>

using namespace Client;

#define BUF_SIZE    2048

TCPConnectorObject::TCPConnectorObject(const Parameters &params) :
    AbstractTCPConnector(params),
    m_context(NULL), m_requester(NULL), m_connected(false)
{
    m_dataProvider = new FakeDataProvider;
}

bool TCPConnectorObject::isConnected() const
{
    return m_connected && m_context && m_requester;
}

int TCPConnectorObject::lastErrorCode()
{
    return zmq_errno();
}

void TCPConnectorObject::processResponse(const void *data, int sz)
{
    Account acc;

    if (acc.ParseFromArray(data, sz))
        std::cout << "Response from server: " << acc.id() << '\t' << acc.clientname() << '\t' << acc.balance() << std::endl;
}

void TCPConnectorObject::abort()
{
    delete m_response; m_response = NULL;

    if (m_requester)
        zmq_close(m_requester);

    if (m_context)
        zmq_ctx_destroy(m_context);

    m_context = NULL;
    m_requester = NULL;
    m_connected = false;
}

void TCPConnectorObject::connectToServer()
{
    if (m_context == NULL)
        m_context = zmq_ctx_new();

    if (m_requester == NULL)
        m_requester = zmq_socket(m_context, ZMQ_REQ);

    m_connected = (zmq_connect(m_requester, getParameters().connectAddr.c_str()) == 0);

    // This can be placed in a separate method (for example in a virtual reimplemented method onConnectedToServer() that can be called in the base class).
    if (m_response == NULL && m_requester)
        m_response = new ResponseObject(m_requester);

    if (m_connected)
        std::cout << "Client connected" << std::endl;
}

TCPConnectorObject::~TCPConnectorObject()
{
    abort();
    delete m_dataProvider;
}
