rmdir .build /s /q
mkdir .build
cd .build
set CL=/MP
cmake -DCMAKE_BUILD_TYPE=Release -G "NMake Makefiles" ../
nmake
cd ../