#ifndef RESPONSEOBJECT_H
#define RESPONSEOBJECT_H

#include "AbstractResponseObject.h"

namespace Client {

class ResponseObject : public AbstractResponseObject
{
public:
    ResponseObject(void *requester);
    virtual ~ResponseObject();

    bool send(const void *data, int sz);
    void getResponse(void *data, int *sz);

private:
    void *m_requester;
    void *m_buf_recv;
    int m_lastReadSz;
};

}

#endif // RESPONSEOBJECT_H
