#ifndef ABSTRACTTCPCONNECTOR_H
#define ABSTRACTTCPCONNECTOR_H

#include "Parameters.h"

namespace Client {

class AbstractDataProvider;
class AbstractResponseObject;

class AbstractTCPConnector
{
public:
    AbstractTCPConnector(const Parameters &params);
    virtual ~AbstractTCPConnector();

    void setDataProvider(AbstractDataProvider *dataProvider);
    void setResponseObject(AbstractResponseObject *response);

    int run();

    virtual bool isConnected() const = 0;

    virtual void abort() = 0;

protected:
    const Parameters &getParameters() const;

private:
    virtual void connectToServer() = 0;
    virtual int lastErrorCode() = 0;
    virtual void processResponse(const void *data, int sz) = 0;

protected:
    AbstractDataProvider *m_dataProvider;
    AbstractResponseObject *m_response;

private:
    Parameters m_params;
};

}

#endif // ABSTRACTTCPCONNECTOR_H
