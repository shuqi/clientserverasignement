#ifndef TCPCONNECTOROBJECT_H
#define TCPCONNECTOROBJECT_H

#include "AbstractTCPConnector.h"

namespace Client {

class TCPConnectorObject : public AbstractTCPConnector
{
public:
    TCPConnectorObject(const Parameters &params);
    virtual ~TCPConnectorObject();

    bool isConnected() const;

    void abort();

private:
    void connectToServer();
    int lastErrorCode();
    void processResponse(const void *data, int sz);

private:
    void *m_context;
    void *m_requester;
    bool m_connected;
};

}

#endif // TCPCONNECTOROBJECT_H
