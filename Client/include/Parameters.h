#ifndef CLIENTPARAMETERS_H
#define CLIENTPARAMETERS_H

#include <string>

namespace Client {

struct Parameters
{
    std::string connectAddr;
};

}

#endif // CLIENTPARAMETERS_H
