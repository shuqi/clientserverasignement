#ifndef FAKEDATAPROVIDER_H
#define FAKEDATAPROVIDER_H

#include "AbstractDataProvider.h"
#include "account.pb.h"

#include <queue>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>

class Account;

namespace Client {

class FakeDataProvider : public AbstractDataProvider
{
public:
    FakeDataProvider();
    virtual ~FakeDataProvider();

    bool open();

    bool isOpen();

    // Returns 0 if success
    int read(void *data, int *sz);

    void close();

private:
    void run();
    void addAccount(const Account &acc);

private:
    boost::thread *m_thread;
    boost::mutex m_mutex;
    boost::condition m_condition;

    std::queue<Account> m_accounts;
    bool m_open;
    bool m_canRead;
};

}

#endif // FAKEDATAPROVIDER_H
