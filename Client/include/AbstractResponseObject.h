#ifndef ABSTRACTRESPONSEOBJECT_H
#define ABSTRACTRESPONSEOBJECT_H

namespace Client {

class AbstractResponseObject
{
public:
    virtual bool send(const void *data, int sz) = 0;
    virtual void getResponse(void *data, int *sz) = 0;
};

}

#endif // ABSTRACTRESPONSEOBJECT_H
