#ifndef ABSTRACTDATAPROVIDER_H
#define ABSTRACTDATAPROVIDER_H

namespace Client {

class AbstractDataProvider
{
public:
    virtual bool open() = 0;

    virtual bool isOpen() = 0;

    /// Returns zero if is possible to continue reading data.
    virtual int read(void *data, int *sz) = 0;

    virtual void close() = 0;
};

}

#endif // ABSTRACTDATAPROVIDER_H
