message("Including package_boost.cmake ...")

set(BOOST_ROOT "${EXTERNALS_ROOT}/boost_1_57_0")
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)

if(POLICY CMP0020)
    cmake_policy(SET CMP0020 NEW)
endif()

find_package(Boost 1.56.0 REQUIRED COMPONENTS system thread date_time chrono)

if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
else()
    print_error ("Please make sure you have version 1.56.0 installed in the same directory as the application." "Boost at ${BOOST_ROOT}")
endif ()
