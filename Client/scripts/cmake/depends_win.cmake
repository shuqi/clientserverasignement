message("Including dependents_win.cmake ...")

set(APP_CXX_FLAGS "${APP_CXX_FLAGS} /MP")
set(APP_EXE_LINKER_FLAGS "")

#set(PLATFORM_SPECIFIC_RESOURCE_FILE "res.rc")
set(PLATFORM_SPECIFIC_INCLUDE_DIR)
set(excluded_HEADER_FILES_REGEX "^.*(_mac|_unix|.*/mac/.*).h$")
set(excluded_SOURCE_FILES_REGEX "^.*(_mac|_unix|.*/mac/.*).cpp$")

set(PLATFORM_SPECIFIC_SOURCE_FILES)
set(PLATFORM_SPECIFIC_DEFS -D_WIN32_WINDOWS -D_CRT_SECURE_NO_WARNINGS)
